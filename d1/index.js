// console.log('Hellow Wordls');

	let studentNumberA = "2022-1923";
	let studentNumberB = "2022-1924";
	let studentNumberC = "2022-1925";
	let studentNumberD = "2022-1926";

	//Array is simply a list of data
	let studentNumbers = ["2022-1923", "2022-1924", "2022-1925", "2022-1926"];
	console.log(studentNumbers);

	let grades = [98.5, 93.3, 89.2, 90.1];
	console.log(grades);

	let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Apple', 'Dell', 'Samsung', 'Toshiba'];
	console.log(computerBrands);

	let persons = ['Jane', 'Smith', 12, true, null, undefined, {}];
	console.log(persons);

	let myTasks = [
		'drink html',
		'east javascript',
		'inhale css',
		'bake bootscrap'
	];
	console.log(myTasks);


	//Creating an array with values from variables:

	let city1 = 'Tokyo';
	let city2 = 'Manila';
	let city3 = 'Jakarta';

	let cities = [city1, city2, city3];
	console.log(cities);

	// [ SECTION ]  .length property

	console.log(myTasks.length);

	console.log(cities.length);

	let blankArr = [];
	console.log(blankArr.length);

	let fullName = 'Janssen Joy Jocson';
	console.log(fullName.length);


	//Lenght property can also set the total number of items in an array, meaning we can delete the in the array
	myTasks.length = myTasks.length-1;
	console.log(myTasks.length);
	console.log(myTasks);

	cities.length--;
	console.log(cities);

	fullName.length = fullName.length-1;
	console.log(fullName.length);

	let theBeatles = ['John', 'Paul', "Ringo", 'George'];
	theBeatles.length++;
	console.log(theBeatles);

//[ SECTION ] Reading from Arrays/Accessing Elements in an Array

	console.log(grades[3]);
	console.log(computerBrands[5]);
	console.log(theBeatles[3]);

	//Acessing an array element that does not exist will return undefined
	console.log(grades[10]);


	let lakerLegends = ['Kobe', 'Kareem', 'Magic', 'Shaq', 'LeBron'];
	console.log(lakerLegends[3]);
	console.log(lakerLegends[4]);

	//You can also store an array item in another variable
	let currentLaker = lakerLegends[0];
	console.log(currentLaker);

	console.log('Array before reassignment: ');
	console.log(lakerLegends);
	lakerLegends[2] = "Pau Gasol";
	console.log('Array after reassignment: ');
	console.log(lakerLegends);


	let bullsLegends = ['Jordan', 'Rose', 'Pipen', 'Rodman', 'Kukoc'];
	let lastElementIndex = bullsLegends.length-1;
	console.log(bullsLegends[lastElementIndex]);
	console.log(bullsLegends[bullsLegends.length-1]);


//Adding Items into an array
	let newArr = [];
	console.log(newArr[0]);

	newArr[0] = 'Cloud Strife';
	console.log(newArr);

	newArr[1] = 'Tifa Lockhart';
	console.log(newArr);

	// You can also add items at the of an array.
	newArr[newArr.length] = 'Barrett Wallace';
	console.log(newArr);

	//Loopin over an Array
	//We can use a for loop to iterate over all items in an array
	for (let index = 0; index < newArr.length; index ++){
		console.log(newArr[index]);
	}

	let numArr = [5, 12, 30, 46, 40];

	for( let i = 0; i < numArr.length; i++){
		if ( numArr[i] % 5 === 0) {
			console.log(numArr[i] + ' is divisible by 5');
		} else {
			console.log(numArr[i]+ ' is not divisible by 5');
		}
	};


// [SECTION] Multidimensional Arrays

	let chessBoard = [
				["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
				["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
				["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
				["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
				["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
				["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
				["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
				["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
	];

	console.table(chessBoard);

	console.log(chessBoard[1][4]);
	console.log(chessBoard[6][7]);

	console.log('Pawn moves to: ' + chessBoard[1][5]);














